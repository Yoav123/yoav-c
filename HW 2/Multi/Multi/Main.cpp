#include <stdio.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <iostream>
#include <vector>

#define MESSAGE_LEN 2222

int ContinueOfServer(int tcp_socket, char recieved_message[MESSAGE_LEN], int FLAGS){

	if (recv(tcp_socket, recieved_message, MESSAGE_LEN, FLAGS) == SOCKET_ERROR)
	{
		printf("An error occurred: messege was not recieved.\n");
		closesocket(tcp_socket);
		WSACleanup();
		system("PAUSE");
		return 0;
	}
	printf("Recieved message: %s\n", recieved_message);
	if (send(tcp_socket, recieved_message, strlen(recieved_message) + 1, FLAGS) == SOCKET_ERROR)
	{
		printf("An error occurred: messege was not sent.\n");
		closesocket(tcp_socket);
		WSACleanup();
	}
	closesocket(tcp_socket);
	WSACleanup();
	return 1;
}

int main(){
	std::vector<std::thread> Threads;

	for (;;){
		char *LOCAL_ADDRESS = "0.0.0.0"; //The IP for the server - that means that include all the ips
		char recieved_message[MESSAGE_LEN];
		const int CONNECTION_TYPE = SOCK_STREAM;
		const int PROTOCOL = IPPROTO_TCP;
		const int PORT = 6713;
		const int FLAGS = 0;
		WSADATA info;
		if (WSAStartup(MAKEWORD(2, 0), &info))
		{
			printf("An error occurred: cannot configure a socket.\n");
			system("PAUSE");
			return 0;
		}
		int tcp_socket;
		if ((tcp_socket = socket(AF_INET, CONNECTION_TYPE, PROTOCOL)) == INVALID_SOCKET)
		{
			printf("An error occurred: cannot create a socket.\n");
			WSACleanup();
			system("PAUSE");
			return 0;
		}
		sockaddr_in server;
		server.sin_family = AF_INET;
		server.sin_addr.s_addr = inet_addr(LOCAL_ADDRESS);
		server.sin_port = htons(PORT);
		if (bind(tcp_socket, (sockaddr*)&server, sizeof server))
		{
			printf("An error occurred: cannot associate a socket with an IPv4 address.\n");
			closesocket(tcp_socket);
			WSACleanup();
			system("PAUSE");
			return 0;
		}
		if (listen(tcp_socket, SOMAXCONN))
		{
			printf("An error occurred: cannot place a socket in a listening state.\n");
			closesocket(tcp_socket);
			WSACleanup();
			system("PAUSE");
			return 0;
		}
		if ((tcp_socket = accept(tcp_socket, NULL, NULL)) == INVALID_SOCKET)
		{
			printf("An error occurred: cannot accept incoming connection.\n");
			closesocket(tcp_socket);
			WSACleanup();
			system("PAUSE");
			return 0;
		}
		std::thread temp(ContinueOfServer, tcp_socket, recieved_message, FLAGS);
		Threads.push_back(std::thread(std::move(temp)));

	}
	system("PAUSE");
	return 0;
}